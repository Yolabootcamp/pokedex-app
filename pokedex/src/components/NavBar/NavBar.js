import React from "react";
import { Nav, NavLink, NavMenu } from "./NavbarElements";

export default function NavBar() {
  return (
    <>
      <Nav>
        <NavMenu>
          <NavLink to="/pokemoninfo" activeStyle>
            PokemonInfo
          </NavLink>
          <NavLink to="/pokemoncards" activeStyle>
            PokemonCards
          </NavLink>
        </NavMenu>
      </Nav>
    </>
  );
}
