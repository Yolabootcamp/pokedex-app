import React from "react";
import "../../styles/style.css";

export default function Home() {
  return (
    <div className="home-page">
      <h1>Welcome to my awesome Pokédex App</h1>
    </div>
  );
}
