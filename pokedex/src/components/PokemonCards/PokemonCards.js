import axios from "axios";
import React, { useState, useEffect } from "react";
import Cards from "../Cards/Cards";
import CardGroup from "react-bootstrap/CardGroup";


export default function PokemonCards() {
  const [pokemons, setPokemons] = useState([]);

  /* function arrayPokemon(results) {
    results.forEach(async (poke) => {
      const res = await fetch(`https://pokeapi.co/api/v2/pokemon/${poke.name}`);
      const data = await res.json();

      setPokemons((currentArray) =>
        [...currentArray, data].sort((a, b) => (a.id > b.id ? 1 : -1))
      );
    });
  } */

  useEffect(() => {
    axios
      .get(`https://pokeapi.co/api/v2/pokemon?limit=9`)
      .then((response) => {
        return response.data.results;
      })
      .then((results) => {
        return Promise.all(results.map((response) => axios.get(response.url)));
      })
      .then((results) => {
        setPokemons(results.map((response) => response.data));
      });
  }, []);

  return (
    <div className="cards-container">
      {pokemons.map((pokemon) => (
        <div className='cards'>
        <Cards
          key={pokemon.id}
          name={pokemon.name}
          species={pokemon.name}
          image={pokemon.sprites.front_default}
          attack={pokemon.stats[1].base_stat}
          defense={pokemon.stats[2].base_stat}
          speed={pokemon.stats[5].base_stat}
          type={pokemon.types[0].type.name}
          />
        </div>
      ))}
    </div>
  );
}
