import React from "react";
import { NavLink } from "react-router-dom";
import { UilInfo } from "@iconscout/react-unicons";
import { UilCardAtm } from "@iconscout/react-unicons";
import { UilEstate } from "@iconscout/react-unicons";

export default function Header() {
  return (
    <nav className="navbar">
      <h1>Pokémon App</h1>
      <div className="navlinkdiv">
        <div className="link-container">
          <NavLink to="/" activeClassName="active">
            <UilEstate />
            Home Page
          </NavLink>
          <NavLink to="/pokemon-info" activeClassName="active">
            <UilInfo /> Pokemon Info
          </NavLink>
          <NavLink to="/pokemon-cards" activeClassName="active">
            <UilCardAtm /> Pokemon Cards
          </NavLink>
        </div>
      </div>
    </nav>
  );
}
