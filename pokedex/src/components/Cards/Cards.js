import React from "react";
import Card from "react-bootstrap/Card";
import { Col, Container, Row } from "react-bootstrap";

export default function Cards(props) {
  return (
    <div classname="cards-text">
      <Container>
        <Row>
          <Col>
            <Card style={{ width: "18rem" }}>
              <Card.Img variant="top" src={props.image} />
              <Card.Body>
                <Card.Title>{props.name}</Card.Title>
                <Card.Text>
                  <p>Species: {props.name}</p>
                  <p>Attack: {props.attack}</p>
                  <p>Defense:{props.defense}</p>
                  <p>Speed: {props.speed}</p>
                  <p>Type: {props.type}</p>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
