import "./styles/style.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import PokemonInfo from "./components/PokemonInfo/PokemonInfo";
import PokemonCards from "./components/PokemonCards/PokemonCards";
import HomePage from "./components/HomePage/HomePage";
import Header from "../src/components/Header/Header";
import Footer from "../src/components/Footer/Footer";

function App() {
  return (
    <div>
      <Router>
        <div>
          <Header />
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route path="/pokemon-info" component={PokemonInfo} />
            <Route path="/pokemon-cards" component={PokemonCards} />
          </Switch>
          <Footer />
        </div>
      </Router>
    </div>
  );
}

export default App;
